import torchvision.datasets
from torch import nn
from torch.nn import Sequential, Conv2d, MaxPool2d, Flatten, Linear
from torch.utils.data import DataLoader

from nn_loss import loss

dataset = torchvision.datasets.CIFAR10("../data", train=False, download=False,transform=torchvision.transforms.ToTensor())
dataloader = DataLoader(dataset,batch_size=1)

class Tudui(nn.Module):
    def __init__(self) :
        super(Tudui,self).__init__()
        self.model1 = Sequential(
            Conv2d(3, 32, 5, padding=2),
            MaxPool2d(2),
            Conv2d(32, 32, 5, padding=2),
            MaxPool2d(2),
            Conv2d(32, 64, 5, padding=2),
            MaxPool2d(2),
            Flatten(),
            Linear(1024, 64),
            Linear(64, 10)
        )

    def forward(self,x):
        x=self.model1(x)
        return x

tudui = Tudui()
for data in dataloader:
    imgs, targets=data
    outputs = tudui(imgs)
    result_loss = loss(outputs,targets)
    result_loss.backward()
    print((result_loss))